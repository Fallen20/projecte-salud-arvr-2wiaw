const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");

module.exports = {
  mode: "development",
  devServer: {
    historyApiFallback: true,
    https: true,
    open: true,
    compress: true,
    hot: true,
    port: 8000,
  },
  entry: {
    cones: path.resolve(__dirname, "./src/js/cones.js"),
    chair: path.resolve(__dirname, "./src/js/chair.js"),
    raycaster: path.resolve(__dirname, "./src/js/raycaster.js"),
    videoTexture: path.resolve(__dirname, "./src/js/videoTexture.js"),
    Memory: path.resolve(__dirname, "./src/js/Memory.js"),
    Nico: path.resolve(__dirname, "./src/js/raycasterNico.js"),
  },
  output: {
    path: path.resolve(__dirname, "./dist"),
    filename: "[name].bundle.js",
    //publicPath: 'static/',
  },
  //esto te crea la web sola
  plugins: [
    new HtmlWebpackPlugin({
      template: path.resolve(__dirname, "./src/home.html"),
      filename: "home.html",
      inject: false,
    }),
    new HtmlWebpackPlugin({
      template: path.resolve(__dirname, "./src/index.html"),
      filename: "index.html",
      inject: false,//esto es par que no salga lo del boton de start AR
    }),
    new HtmlWebpackPlugin({
      template: path.resolve(__dirname, "./src/rankingMemory.html"),
      filename: "rankingMemory.html",
      inject: false,//esto es par que no salga lo del boton de start AR
    }),
    new HtmlWebpackPlugin({
      template: path.resolve(__dirname, "./src/rankingPuzzle.html"),
      filename: "rankingPuzzle.html",
      inject: false,//esto es par que no salga lo del boton de start AR
    }),
    new HtmlWebpackPlugin({
      template: path.resolve(__dirname, "./src/base.html"),
      filename: "cones.html",
      chunks: ["cones"],
    }),
    new HtmlWebpackPlugin({
      template: path.resolve(__dirname, "./src/base.html"),
      filename: "chair.html",
      chunks: ["chair"],
    }),
    new HtmlWebpackPlugin({
      template: path.resolve(__dirname, "./src/base.html"),
      filename: "raycaster.html",
      chunks: ["raycaster"],
    }),
    new HtmlWebpackPlugin({
      template: path.resolve(__dirname, "./src/baseVideo.html"),
      filename: "videoTexture.html",
      chunks: ["videoTexture"],
    }),
    new HtmlWebpackPlugin({
      template: path.resolve(__dirname, "./src/base.html"),
      filename: "Memory.html",
      chunks: ["Memory"],
    }),
    new HtmlWebpackPlugin({
      template: path.resolve(__dirname, "./src/nicobase.html"),
      filename: "Nico.html",
      chunks: ["Nico"],
    }),
  ],
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
      },
      {
        test: /\.glb|png|jpg/,
        type: "asset/resource",
      },
      {
        test: /\.(mp4|ogv)/,
        type: "asset/resource",
      },
      {
        test: /\.mind/,
        type: "asset/resource",
      },
      {
        test: /\.(css)$/,
        use: ["style-loader", "css-loader"],
      },
    ],
  },
  //    watch: true
};
