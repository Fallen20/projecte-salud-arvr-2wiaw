/*Otras librerias qu epodriamos cargar*/
//import * as ThreeMeshUI from "three-mesh-ui";
//import * as THREEx from '@ar-js-org/ar.js/three.js/build/ar-threex.js';

import XRScene from "./views/XRscene";
import { cubo3D } from "./models/geometryMusic";

import videoSintel2 from "../videos/sintel.mp4";
import videoPrueba from "../videos/gandalf.mp4";
import mario from "../videos/mario.mp4";
import drum from "../videos/drum.mp4";
import errorSound from "../videos/error.mp4";
import base from "../videos/base.mp4";

import "../css/buttonStyles.css";
import "../css/styleMusic.css";

import { ObjectControllerMusic } from "./controllers/ObjectControllerMusic.js";

let arrayVideos = [videoPrueba, videoSintel2, mario, drum, errorSound, base];

//Cargamos la SCENA
const xrScene = new XRScene();
// Inicializamos tanto los videos como los cubos en arrays.
const videoEl = [];
const cubo = [];
/*
En el objetto de la scena xrScene tenemos todos los elementos necesarios
xrScene.scene: Para añadir elementos
xrScene.controller: Para manerjar los eventos del usuario
xrScene.camera: Para cambiar parametros de la camara
*/

let cantidad=5;
for (let i = 0; i < arrayVideos.length; i++) {
  videoEl[i] = document.getElementById("video" + i);
}

for (let i = 0; i < arrayVideos.length; i++) {
  videoEl[i].children[0].src = arrayVideos[i];
}

for (let i = 0; i <= cantidad; i++) {
  cubo[i] = new cubo3D(videoEl[i]);
}

// Le ponemos una posicion (x, y, z), si Z es negativo, aparece delante nuestro, si es positivo, aparece detras
let z = -40;
let x = 10;
let y = -20;
let a = 0;
for (let i = 0; i <= cantidad; i++) {
  if (i % 2 == 0) x = 10;
  else x = -10;
  cubo[i].setPosition(x, y, z, xrScene.controller.matrixWorld);
  a++;
  if (a % 2 == 0) y += 20;
}

// Añadimos los cubos a la escena.
for (let i = 0; i <= cantidad; i++) {
  xrScene.scene.add(cubo[i].get());
}

// Cargamos los videos.
for (let i = 0; i <= cantidad; i++) {
  videoEl[i].load();
}

for (let i = 0; i <= cantidad; i++) {
  new ObjectControllerMusic(xrScene, cubo[i]);
}



//-------------------
document.getElementById("boton-instrucciones").addEventListener("click", show);

function show(){

  let div=document.getElementById("infoDiv");
  div.hidden = !div.hidden;

  // console.log(div)


  // console.log(div.getAttribute("hidden"));
    // if (!div.hasAttribute("hidden")) {
    //   div.setAttribute("hidden", true);
    // } else {
    //   div.removeAttribute("hidden");
    // }
  

}