/*Otras librerias qu epodriamos cargar*/
//import * as ThreeMeshUI from "three-mesh-ui";
//import * as THREEx from '@ar-js-org/ar.js/three.js/build/ar-threex.js';

import XRScene from './views/XRscene';

import { object3D } from './models/gltf';
import sillonFile from '../3d/sillon.glb';


//Cargamos la SCENA
const xrScene = new XRScene();
/*
En el objetto de la scena xrScene tenemos todos los elementos necesarios
xrScene.scene: Para añadir elementos
xrScene.controller: Para manerjar los eventos del usuario
xrScene.camera: Para cambiar parametros de la camara
*/

/*Mostramos la caja de texto para comprobar que podemos superponer elementos DOM*/
document.getElementById('dom-box').style.display= "block";

//Insertamos un objeto3D en el punto 0,0,0 respecto el usuario cada cez que hace click a un boton
//Como tenemos que leerlo de un fichero, la funcion debe ser asyncrona
document.getElementById('addSilla').addEventListener("click", () => {
	loadSillon(0, -0.3,  -0.3, xrScene.controller.matrixWorld);
})

async function loadSillon(x, y, z, matrix) {
	var sillon = new object3D(sillonFile);
	await sillon.load()
	sillon.setPosition(x, y, z, matrix);
	sillon.setSize(0.2,0.2,0.2)
	xrScene.scene.add(sillon.get());
}