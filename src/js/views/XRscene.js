//https://gist.github.com/mcwachanga/6423e9d935f725f602e5ed9e592ca5c4
//https://felixmariotto.github.io/three-mesh-ui/#interactive_button

import * as THREE from "three";
import { ARButton } from "three/examples/jsm/webxr/ARButton.js";
import * as TWEEN from "tween";

export default class XRScene {
  constructor() {
    this.xrSession = null;
    this.xrRefSpace = null;
    this.intersects = [];
    this.onRenderFcts = [];
    this.gl = null;
    this.loadApp = function () {};

    this.scene = new THREE.Scene();
    this.camera = new THREE.PerspectiveCamera(50, window.innerWidth / window.innerHeight, 0.1, 100);
    this.raycaster = new THREE.Raycaster();

    //this.camera.matrixAutoUpdate = false;

    const light = new THREE.HemisphereLight(0xffffff, 0xbbbbff, 1);
    light.position.set(0.5, 1, 0.25);
    this.scene.add(light);

    this.renderer = new THREE.WebGLRenderer({ antialias: true, alpha: true });
    this.renderer.setSize(window.innerWidth, window.innerHeight);
    this.renderer.setPixelRatio(window.devicePixelRatio);
    this.renderer.domElement.style.position = "absolute";
    this.renderer.domElement.style.top = "0px";
    this.renderer.domElement.style.left = "0px";
    this.renderer.xr.enabled = true;
    this.controller = this.renderer.xr.getController(0);
    this.scene.add(this.controller);

    let options = {
      //requiredFeatures: ['hit-test'],
      optionalFeatures: ["dom-overlay", "dom-overlay-for-handheld-ar"],
      domOverlay: { root: document.getElementById("overlay") },
    };
    document.body.appendChild(ARButton.createButton(this.renderer, options));
    window.addEventListener("resize", this.onWindowResize.bind(this));

    //RAYVAST
    document.addEventListener("pointerdown", this.onPointerMove.bind(this));

    this.renderScene();
  }

  renderScene() {
    var self = this;
    var onRenderFcts = this.onRenderFcts;
    this.onRenderFcts.push(function () {
      self.renderer.setAnimationLoop(() => self.renderer.render(self.scene, self.camera));
    });

    var lastTimeMsec = null;
    requestAnimationFrame(function animate(nowMsec) {
      // keep looping
      requestAnimationFrame(animate);
      TWEEN.update();
      // measure time
      lastTimeMsec = lastTimeMsec || nowMsec - 1000 / 60;
      var deltaMsec = Math.min(200, nowMsec - lastTimeMsec);
      onRenderFcts.forEach(function (onRenderFct) {
        onRenderFct(deltaMsec / 1000, nowMsec / 1000);
      });
    });
  }

  onWindowResize() {
    this.camera.aspect = window.innerWidth / window.innerHeight;
    this.camera.updateProjectionMatrix();
    this.renderer.setSize(window.innerWidth, window.innerHeight);
  }
  /*
   * Cada vez que movemos el raton calculamos qué objetos interaccionan con el puntero (dedo)
   * this.intersects guarda un array con los objetos que entran en contacto con el dedo
   */
  onPointerMove(event) {
    var pointer = new THREE.Vector2();
    pointer.x = (event.clientX / window.innerWidth) * 2 - 1;
    pointer.y = -(event.clientY / window.innerHeight) * 2 + 1;

    // update the picking ray with the camera and pointer position
    this.raycaster.setFromCamera(pointer, this.camera);
    // calculate objects intersecting the picking ray
    this.intersects = this.raycaster.intersectObjects(this.scene.children);
  }
}

// VERSION NUEVA
// /*Otras librerias qu epodriamos cargar*/
// //import * as ThreeMeshUI from "three-mesh-ui";
// //https://gist.github.com/mcwachanga/6423e9d935f725f602e5ed9e592ca5c4
// //https://felixmariotto.github.io/three-mesh-ui/#interactive_button

// import * as THREE from 'three';
// import { ARButton } from 'three/examples/jsm/webxr/ARButton.js';

// export default class XRScene {

//     constructor() {
//         this.xrSession = null;
//         this.xrRefSpace = null;
//         this.intersects = [];
//         this.onRenderFcts = [];
//         this.arToolkitSource = null;
//         this.arToolkitContext = null;
//         this.mixers = [];

//         this.gl = null;
//         this.loadApp = function () { };

//         this.scene = new THREE.Scene();

//         this.camera = new THREE.PerspectiveCamera(30, window.innerWidth / window.innerHeight, 0.01, 100);
//         //this.camera = new THREE.Camera();
//         this.raycaster = new THREE.Raycaster();
//         //this.camera.matrixAutoUpdate = false;

//         const light = new THREE.HemisphereLight(0xffffff, 0xbbbbff, 1);
//         light.position.set(0.5, 1, 0.25);
//         this.scene.add(light);

//         this.renderer = new THREE.WebGLRenderer({ antialias: true, alpha: true });
//         this.renderer.setSize(window.innerWidth, window.innerHeight);
//         this.renderer.setPixelRatio(window.devicePixelRatio);
//         this.renderer.domElement.style.position = 'absolute'
//         this.renderer.domElement.style.top = '0px'
//         this.renderer.domElement.style.left = '0px'
//         this.renderer.xr.enabled = true;
//         this.controller = this.renderer.xr.getController(0);

//         window.addEventListener('resize', this.onWindowResize.bind(this));
//         window.addEventListener('pointerover', this.onPointerMove.bind(this));

//         this.renderScene();
//     }
//     renderScene() {
//         var self = this
//         var onRenderFcts = this.onRenderFcts
//         this.scene.add(this.controller);
//         let options = {
//             //requiredFeatures: ['hit-test'],
//             // optionalFeatures: ['dom-overlay', 'dom-overlay-for-handheld-ar','image-tracking'],
//             optionalFeatures: ['dom-overlay', 'dom-overlay-for-handheld-ar'],
//             domOverlay: { root: document.getElementById('overlay') }
//         }
//         document.body.appendChild(ARButton.createButton(this.renderer, options));
//         this.onRenderFcts.push(function () {
//             self.renderer.setAnimationLoop(() => self.renderer.render(self.scene, self.camera));
//         })

//         var lastTimeMsec = null
//         var clock = new THREE.Clock();
//         requestAnimationFrame(function animate(nowMsec) {
//             // keep looping
//             requestAnimationFrame(animate);
//             if (self.mixers.length > 0) {
//                 for (var i = 0; i < self.mixers.length; i++) {
//                     self.mixers[i].update(clock.getDelta());
//                 }
//             }
//             // measure time
//             lastTimeMsec = lastTimeMsec || nowMsec - 1000 / 60
//             var deltaMsec = Math.min(200, nowMsec - lastTimeMsec)
//             onRenderFcts.forEach(function (onRenderFct) {
//                 onRenderFct(deltaMsec / 1000, nowMsec / 1000)
//             })
//         })
//     }

//     onWindowResize() {
//         this.camera.aspect = window.innerWidth / window.innerHeight;
//         this.camera.updateProjectionMatrix();
//         this.renderer.setSize(window.innerWidth, window.innerHeight);

//         //TRACKING Resize
//         if (this.arToolkitSource) {
//             this.arToolkitSource.onResizeElement()
//             this.arToolkitSource.copyElementSizeTo(this.renderer.domElement)
//             if (this.arToolkitContext.arController !== null) {
//                 this.arToolkitSource.copyElementSizeTo(this.arToolkitContext.arController.canvas)
//             }
//         }
//     }
//     /*
//     * Cada vez que movemos el raton calculamos qué objetos interaccionan con el puntero (dedo)
//     * this.intersects guarda un array con los objetos que entran en contacto con el dedo
//     */
//     onPointerMove(event) {
//         var pointer = new THREE.Vector2();
//         pointer.x = (event.clientX / window.innerWidth) * 2 - 1;
//         pointer.y = - (event.clientY / window.innerHeight) * 2 + 1;

//         /* Other options of raycast */
//         /*
//         var pointer = new THREE.Vector3(0, 0, -10);
//         pointer.x = (event.clientX / window.innerWidth) * 2 - 1;
//         pointer.y = - (event.clientY / window.innerHeight) * 2 + 1;
//         */

//         /*
//         var pointer = new THREE.Vector3(0, 0, -1);
//         pointer = this.XRScene.camera.localToWorld(pointer);
//         pointer.sub(this.XRScene.camera.position);
//         */

//         // update the picking ray with the camera and pointer position
//         this.raycaster.setFromCamera(pointer, this.camera);
//         // calculate objects intersecting the picking ray
//         this.intersects = this.raycaster.intersectObjects(this.scene.children);

//     }

// }
