import * as THREE from "three";


export class cubo3D {
  //esto es una clase
  //y dentro tiene metodos
  constructor(video) {
    this.video = video;
    this.isPlaying = false;


    var geometry = new THREE.BoxGeometry(10, 10, 10); //creas la forma
    // const material = new THREE.MeshPhongMaterial({ color: 0xffffff * Math.random() });//color. Es uno random

    //creamos textura
    let texture = new THREE.VideoTexture(video);
    

    //creamos el material
    let materialMovie = new THREE.MeshBasicMaterial({
      map: texture, //darle la textura con el movil
      side: THREE.DoubleSide, //se ve en ambos lados
      toneMapped: false,
    });
    materialMovie.needsUpdate = true;

    this.object = new THREE.Mesh(geometry, materialMovie); //le aplicas esto
  }

  //metodo para ponerlo en una posicion especifica
  setPosition(x, y, z, matrix = null) {
    if (matrix != null) {
      this.object.position.set(x, y, z).applyMatrix4(matrix);
    } else this.object.position.set(x, y, z);

    this.object.visible = true;
  }

  //un get
  get() {
    return this.object;
  }
  getTexture() {
    return this.object.material;
  }

  //Para pillar el video.
  getVideo() {
    return this.video;
  }
  
  //---------------------------------
  checkVideo(){
    if(!this.video.paused){
        this.video.pause();
        return;
    }
    if(this.video.paused){
        this.video.play();
        return;
    }
  }

  pauseVideo(){
    console.log("pause");
    this.video.pause();
    this.isPlaying=false;
  }
  playVideo(){
    console.log("play");
    this.video.play();
    this.isPlaying=true;

  }

}
