import * as THREE from "three";

export class cono3D {
  constructor() {
    var geometry = new THREE.CylinderGeometry(0, 0.05, 0.2, 32).rotateX(Math.PI / 2);
    const material = new THREE.MeshPhongMaterial({ color: 0xffffff * Math.random() });
    this.object = new THREE.Mesh(geometry, material);
  }
  setPosition(x, y, z, matrix = null) {
    if (matrix != null) this.object.position.set(x, y, z).applyMatrix4(matrix);
    else this.object.position.set(x, y, z);
    this.object.visible = true;
  }
  get() {
    return this.object;
  }
}



export class knot3D {
  construct() {
    this.group = new Group();
    this.group.visible = false;
    var mesh = new THREE.AxesHelper();
    this.group.add(mesh);
    // add a torus knot
    var geometry = new THREE.BoxGeometry(1, 1, 1);
    var material = new THREE.MeshNormalMaterial({
      transparent: true,
      opacity: 0.5,
      side: THREE.DoubleSide,
    });
    var mesh = new THREE.Mesh(geometry, material);
    mesh.position.y = geometry.parameters.height / 2;
    this.group.add(mesh);

    geometry = new THREE.TorusKnotGeometry(0.3, 0.1, 64, 16);
    var material = new THREE.MeshNormalMaterial();
    var mesh = new THREE.Mesh(geometry, material);
    mesh.position.y = 0.5;
    this.group.add(mesh);
  }
  setPosition(x, y, z, matrix = null) {
    if (matrix != null) this.group.position(x, y, z).setFromMatrixPosition(matrix);
    else this.group.position.set(x, y, z);
    this.group.visible = true;
  }
  get() {
    return this.group;
  }
}
