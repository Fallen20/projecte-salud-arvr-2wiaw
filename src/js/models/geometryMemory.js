import * as THREE from "three";

// import { convertArray } from "three/src/animation/AnimationUtils";

export class plano3D {
    constructor(frente, atras,scene) {
      this.isRotated = false;
      this.canRotate=true;
      this.array=[];
      this.arrayId=[];
      this.xrScene=scene;

      
      const geometry = new THREE.BoxGeometry(2, 4, 1); //creamos
  
      //creamos texturas
      let back = this.setTexture(atras);
      let front = this.setTexture(frente);
      let sides = this.setSideTexture();
  
      var cubeMaterialArray = [sides, sides, sides, sides, front, back];
      // var cubeMaterialArray = [sides,sides,sides,sides,sides,sides];
  
      this.object = new THREE.Mesh(geometry, cubeMaterialArray);
      // this.object = new THREE.Mesh(geometry, back);
    }
  
    setTexture(imagen) {
      const textura = new THREE.TextureLoader().load(imagen);
      const material = new THREE.MeshBasicMaterial({ map: textura });
  
      return material;
      // return new THREE.MeshBasicMaterial( {color: 0x000000} );//negro
    }
  
    setSideTexture() {
      return new THREE.MeshBasicMaterial({ color: 0x00000 }); //magenta
    }
  
    giveId(num) {
      this.id = num;
    }
  
    //metodo para ponerlo en una posicion especifica
    setPosition(x, y, z, matrix = null) {
      if (matrix != null) {
        this.object.position.set(x, y, z).applyMatrix4(matrix);
      } else this.object.position.set(x, y, z);
  
      this.object.visible = true;
    }
  
    //un get
    get() {
      return this.object;
    }
  
    animateObject(controler){

     // if(this.arrayId==2){this.checkArray(this.arrayId);}
      
      if(!this.isRotated && this.canRotate){this.animate(controler);}
      else if(this.isRotated && this.canRotate){this.animateReverse(controler);}
    }
    
    animate(controler) {
      
      this.xrScene.onRenderFcts[this.xrScene.onRenderFcts.length] = function (delta, now) {
        
        if (this.object.rotation.y < 3.2){
          this.object.rotation.y += 1 * delta / 2;
        }
        else {
          this.xrScene.onRenderFcts.pop();
        }
      }.bind(this);
      
      this.isRotated=true;

      // this.object.rotation.y=3.2;
      
      controler.addId(this);
    }
    
    animateReverse(controler) {
    
      this.xrScene.onRenderFcts[this.xrScene.onRenderFcts.length] = function (delta, now) {
        if (this.object.rotation.y > 0) {
          this.object.rotation.y -= 1 * delta/2;
        }
        else {
          this.xrScene.onRenderFcts.pop();
        }
      }.bind(this);
      this.isRotated = false;
    
      controler.removeId(this);
    }
    
    
    animateReverseExternal(figura) {
      // console.log("inverso orto");
      figura.isRotated=false;


      this.xrScene.onRenderFcts[this.xrScene.onRenderFcts.length] = (function (delta, now) {
        return function (delta, now) {
          if (figura.object.rotation.y > 0){
            figura.object.rotation.y -= 1 * delta / 2;
          }
          else {
            this.xrScene.onRenderFcts.pop();
          }
        };
      })(figura).bind(this);
      // figura.object.rotation.y=0;
    }
    
    

    checkArray(array){
        self=this;
        let contar=0;
        for (let contador = 0; contador < array.length; contador++) {
            const element = array[contador];
            if(element.plano==this){ element.active=true;}
    
            if(element.active){contar++;}
          }
    
    
        //   hay dos girados, mira que esté bien
          let elem=[];
          if(contar==2){
            elem=array.filter(function(element){
                    return (element.active==true);
                });
          }
          // console.log(elem);
    
          //si hay otro girado y no es el mismo, girar ambos
          //pero los que ya están okay no deben
    
          if(elem.length!=0){
    
            if(elem[0].plano.id==elem[1].plano.id){
                elem[0].plano.canRotate=false;
                elem[1].plano.canRotate=false;
    
                array.splice(array.indexOf(elem[0]), 1);
                array.splice(array.indexOf(elem[1]), 1);
            }
            else{
                elem[0].active=false;
                elem[1].active=false;
                
                // paraque se active al segundo no instantáneo
                setTimeout(function(){
                    self.animateReverseExternal(elem[0].plano);
                    self.animateReverseExternal(elem[1].plano);
                },1000);
            }
        }
    }
  }
