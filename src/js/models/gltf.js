import { GLTFLoader } from 'three/examples/jsm/loaders/GLTFLoader.js';

export class object3D {
    constructor(file) {
        this.file = file
    }
    async load() {
        const gltfData = await this.gltfLoader(this.file);
        this.object = gltfData.scene;
    }
    gltfLoader(file) {
        var loader = new GLTFLoader();
        return new Promise((resolve, reject) => {
            loader.load(file, data => resolve(data), null, reject);
        });
    }
    setPosition(x, y, z, matrix = null) {
        if (matrix != null)
            this.object.position.set(x, y, z).applyMatrix4(matrix);
        //this.object.position.set(x, y, z).setFromMatrixPosition(matrix);
        else this.object.position.set(x, y, z);
        this.object.visible = true;
    }

    setSize(x, y, z) {
        this.object.scale.set(x, y, z);
    }
    get() {
        return this.object;
    }
}