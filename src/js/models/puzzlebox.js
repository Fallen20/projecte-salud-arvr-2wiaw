import * as THREE from "three";
import { ObjectController } from "../controllers/ObjectControllerNico";
import * as funcionesContadores from "./../time_functions.js";


// const folderName = 'puzzle';

// var imgs = []
// for (let i = 1; i <= 8 ; i++) {
//   imgs.push(require(`../../img/${folderName}/${i}.png`));
// }

export class puzzlebox {
  constructor(xrScene, folderName) {
    let imgs = this.loadImgs(folderName);
    this.puzzle = [];

    let images = [];
    const proporcion = 0.1;
    let cont = 0;
    for (let i = 1; i >= -1; i--) {
      for (let j = -1; j <= 1; j++) {
        images.push({ img: imgs[cont], basex: j * proporcion, basey: i * proporcion, num: cont + 1 });
        cont++;
      }
    }

    let random = [0, 1, 2, 3, 4, 5, 6, 7];
    random.sort((a, b) => 0.5 - Math.random());

    xrScene.allObjects = [];
    let count = 0;
    let xSkip = Math.floor(Math.random() * 3) - 1;
    let ySkip = Math.floor(Math.random() * 3) - 1;
    for (let i = 1; i > -2; i--) {
      for (let j = -1; j < 2; j++) {
        if (!(i == ySkip && j == xSkip)) {
          this.createBox(images[random[count]].img);
          this.setPosition(j / 10, i / 10, -0.7);
          this.box.num = images[random[count]].num;
          this.box.homex = images[random[count]].basex;
          this.box.homey = images[random[count]].basey;
          xrScene.scene.add(this.box);
          this.puzzle.push(this.box);
          new ObjectController(xrScene, this.box, this.puzzle);
          count++;
        }
      }
    }
  }
  createBox(img) {
    var geometry = new THREE.BoxGeometry(0.1, 0.1, 0.1).rotateX(Math.PI / 2);
    let textura = new THREE.TextureLoader().load(img);
    let material = new THREE.MeshPhongMaterial({ map: textura });
    this.box = new THREE.Mesh(geometry, material); //cambiar a return
  }
  setPosition(x, y, z, matrix = null) {
    if (matrix != null) this.box.position.set(x, y, z).applyMatrix4(matrix);
    else this.box.position.set(x, y, z);
    this.box.visible = true;
  }
  get() {
    return this.box;
  }
  isSolvable() {
    let count = 0;
    for (let i = 0; i < this.puzzle.length; i++) {
      for (let j = i + 1; j < this.puzzle.length; j++) {
        if (this.puzzle[i].num > this.puzzle[j].num) {
          count += 1;
        }
      }
    }
    return count % 2 == 0;
  }

  isSolved() {
    let solved = this.puzzle.every((a) => {
      return a.position.x == a.homex && a.position.y == a.homey;
    });
    if (solved) {
      this.loadWinText();
    }
  }

  loadImgs(folderName) {
    var imgs = [];
    for (let i = 1; i <= 8; i++) {
      try {
        imgs.push(require(`../../img/puzzle/${folderName}/${i}.jpg`));
      } catch (err) {
        imgs.push(require(`../../img/puzzle/${folderName}/${i}.png`));
      }
    }
    return imgs;
  }

  loadWinText() {
    let texts = [
      "Consejo:<br><br><u>Mantén una dieta equilibrada</u><br><br> Una dieta equilibrada que incluya una variedad de alimentos saludables, como frutas, verduras, granos integrales, proteínas magras y grasas saludables, puede ayudarte a mantener un peso saludable, reducir el riesgo de enfermedades crónicas y mejorar tu bienestar general.",
      "Consejo:<br><br><u>Haz ejercicio regularmente</u><br><br> El ejercicio regular puede ayudarte a mantener un peso saludable, fortalecer tus huesos y músculos, reducir el riesgo de enfermedades crónicas y mejorar tu estado de ánimo y bienestar general. Trata de hacer al menos 150 minutos de actividad física moderada o 75 minutos de actividad física vigorosa a la semana.",
      "Consejo:<br><br><u>Descansa lo suficiente</u><br><br> Dormir lo suficiente es crucial para mantener una buena salud física y mental. Trata de dormir entre 7 y 9 horas cada noche y sigue una rutina regular de sueño para mejorar la calidad de tu sueño.",
      "Consejo:<br><br><u>Reduce el estrés</u><br><br> El estrés crónico puede tener efectos negativos en tu salud física y mental. Busca maneras saludables de reducir el estrés, como meditación, respiración profunda, yoga, ejercicio, pasatiempos relajantes o pasar tiempo con amigos y familiares.",
      "Consejo:<br><br><u>Haz revisiones médicas regulares</u><br><br> Realizarte revisiones médicas regulares puede ayudarte a detectar problemas de salud temprano, antes de que se conviertan en problemas más graves. Asegúrate de hacer chequeos regulares con tu médico y de hacerte pruebas de detección recomendadas para tu edad y sexo.",
      "Consejo:<br><br><u>Limita el consumo de alcohol</u><br><br> El consumo excesivo de alcohol puede tener efectos negativos en tu salud física y mental, incluyendo enfermedades del hígado, aumento de peso, presión arterial alta y problemas mentales. Trata de limitar tu consumo de alcohol y seguir las recomendaciones de consumo saludable.",
      "Consejo:<br><br><u>Deja de fumar</u><br><br> Fumar aumenta el riesgo de enfermedades cardíacas, cáncer y otras enfermedades crónicas. Si eres fumador, intenta dejar de fumar y busca apoyo para lograrlo.",
      "Consejo:<br><br><u>Mantén una buena higiene</u><br><br> Mantener una buena higiene personal puede ayudarte a prevenir la propagación de gérmenes y enfermedades. Lávate las manos regularmente, cubre tu boca y nariz al toser o estornudar y mantén tus espacios personales y tu entorno limpio.",
      "Consejo:<br><br><u>Mantén relaciones saludables</u><br><br> Mantener relaciones saludables con amigos, familiares y seres queridos puede mejorar tu salud mental y emocional. Asegúrate de mantener conexiones significativas y saludables con las personas importantes en tu vida.",
      "Consejo:<br><br><u>Haz tiempo para el autocuidado</u><br><br> El autocuidado es importante para mantener una buena salud física y mental. Haz tiempo para actividades que disfrutes y que te ayuden a reducir el estrés y a cuidar de ti mismo. Esto podría incluir hobbies, tiempo al aire libre, terapia, meditación, masajes u otras actividades que te ayuden a recargar tus baterías.",
    ];
    document.getElementById("p1").innerHTML = texts[Math.floor(Math.random() * texts.length)];

    //ahora no está oculto
    document.getElementById("winP").removeAttribute("hidden");

    //parar el contador
    funcionesContadores.saveLocalstoragePuzzle();
  }
}
