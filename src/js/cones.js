/*Otras librerias qu epodriamos cargar*/
//import * as ThreeMeshUI from "three-mesh-ui";
//import * as THREEx from '@ar-js-org/ar.js/three.js/build/ar-threex.js';

import XRScene from './views/XRscene';
import { cono3D } from './models/geometry';

//Cargamos la SCENA
const xrScene = new XRScene();
/*
En el objetto de la scena xrScene tenemos todos los elementos necesarios
xrScene.scene: Para añadir elementos
xrScene.controller: Para manerjar los eventos del usuario
xrScene.camera: Para cambiar parametros de la camara
*/

//Insertamos un cono3D en el punto 0,0,0 respecto el usuario cada cez que hace click a la pantalla
xrScene.controller.addEventListener("select", () => {
	var cono = new cono3D();
	cono.setPosition(0, 0, - 0.3, xrScene.controller.matrixWorld);
	xrScene.scene.add(cono.get());
})
