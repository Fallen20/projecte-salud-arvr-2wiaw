let contadorPuzzle = 0;//reset, empieza en 0
let contadorMemory = 0;//reset, empieza en 0


let intervalPuzzle;
let intervalMemory;
//-------------------
    //cada segundo suma 1 si no ha ganado

export function iniciarIntervalPuzzle() {
    intervalPuzzle = setInterval(() => {
        contadorPuzzle++;
    }, 1000);
}

    //cada segundo suma 1 si no ha ganado

export function iniciarIntervalMemory(){
    intervalMemory= setInterval(() => {
        contadorMemory++;
}, 1000);
}

//-------------------
export function saveLocalstorageMemory(){
    clearInterval(intervalMemory);
    localStorage.setItem('contadorMemory', contadorMemory);//guarda segundos
}
export function saveLocalstoragePuzzle() {
    clearInterval(intervalPuzzle);
    localStorage.setItem('contadorPuzzle', contadorPuzzle);//guarda segundos
}


