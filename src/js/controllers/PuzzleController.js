import * as TWEEN from "tween";

export class PuzzleController {
  constructor(XRScene, puzzle) {
    this.xrScene = XRScene;
    this.puzzle = puzzle;
  }

  checkDirection(touchstartX, touchendX, touchstartY, touchendY) {
    if (touchendX < touchstartX) {
      if (this.canMove(this.xrScene).left && this.canMoveOut(this.xrScene).left) {
        this.move(-0.1, 0);
      }
    }
    if (touchendX > touchstartX) {
      if (this.canMove(this.xrScene).right && this.canMoveOut(this.xrScene).right) {
        this.move(0.1, 0);
      }
    }
    if (touchendY < touchstartY) {
      if (this.canMove(this.xrScene).up && this.canMoveOut(this.xrScene).up) {
        this.move(0, 0.1);
      }
    }
    if (touchendY > touchstartY) {
      if (this.canMove(this.xrScene).down && this.canMoveOut(this.xrScene).down) {
        this.move(0, -0.1);
      }
    }
  }

  canMoveOut(xrScene) {
    let canMove1 = {};
    canMove1.left = xrScene.selected.position.x - 0.1 > -0.2;
    canMove1.right = xrScene.selected.position.x + 0.1 < 0.2;
    canMove1.down = xrScene.selected.position.y - 0.1 > -0.2;
    canMove1.up = xrScene.selected.position.y + 0.1 < 0.2;
    return canMove1;
  }

  canMove(xrScene) {
    let canMove = {};
    canMove.left = !this.puzzle.puzzle.some(
      (obj) => obj.position.x == xrScene.selected.position.x - 0.1 && obj.position.y == xrScene.selected.position.y
    );

    canMove.right = !this.puzzle.puzzle.some(
      (obj) => obj.position.x == xrScene.selected.position.x + 0.1 && obj.position.y == xrScene.selected.position.y
    );

    canMove.down = !this.puzzle.puzzle.some(
      (obj) => obj.position.x == xrScene.selected.position.x && obj.position.y == xrScene.selected.position.y - 0.1
    );

    canMove.up = !this.puzzle.puzzle.some(
      (obj) => obj.position.x == xrScene.selected.position.x && obj.position.y == xrScene.selected.position.y + 0.1
    );

    canMove.block = this.puzzle.puzzle.some(function (obj) {
      return obj.position != xrScene.selected.position;
    });

    return canMove;
  }

  move(x, y) {
    let puzzle = this.puzzle;
    this.xrScene.selected.tween = new TWEEN.Tween(this.xrScene.selected.position);
    this.xrScene.selected.tween.to(
      {
        x: Math.round((this.xrScene.selected.position.x + x) * 10) / 10,
        y: Math.round((this.xrScene.selected.position.y + y) * 10) / 10,
      },
      100
    );
    this.xrScene.selected.tween.start();
    setTimeout(function () {
      puzzle.isSolved();
    }, 500);
  }
}
