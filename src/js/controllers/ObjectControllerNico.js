export class ObjectController {
  constructor(XRScene, object, puzzle) {
    this.XRScene = XRScene;
    this.object = object;
    this.puzzle = puzzle;

    window.addEventListener("pointerdown", this.onPointerMove.bind(this));
  }

  onPointerMove(event) {
    for (let i = 0; i < this.XRScene.intersects.length; i++) {
      if (this.XRScene.intersects[i].object.uuid == this.object.uuid) {
        this.puzzle.forEach((object, i, array) => {
          array[i].material.emissive.set(0x000000);
        });
        this.XRScene.intersects[i].object.material.emissive.set(0x00ff00);
        this.XRScene.selected = this.XRScene.intersects[i].object;
      }
    }
  }
}
