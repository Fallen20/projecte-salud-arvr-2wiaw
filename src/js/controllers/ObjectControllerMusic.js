// Edu
export class ObjectControllerMusic {
  constructor(XRScene, figura) {
    this.XRScene = XRScene;
    this.figura = figura;
    this.isPlaying = false;

    window.addEventListener('click', this.onPointerMove.bind(this));
  window.addEventListener("pointermove", this.onPointerMove2.bind(this));
  }
  /* Evalua en cada movimiento del raton (dedo) el contenido de this.XRScene.intersects
   *  this.XRScene.intersects se guardan TODOS los objetos que tocan el dedo del usuario
   */

  // Cuando le das tap en el movil haces esto.
  onPointerMove(event) {
    //this.video.load();
    //https://github.com/mrdoob/three.js/blob/master/manual/examples/webxr-point-to-select.html
    for (let i = 0; i < this.XRScene.intersects.length; i++) {
      if (this.XRScene.intersects[i].object.uuid == this.figura.object.uuid) {
        let video = this.figura.getVideo();
        video.play();
        this.isPlaying = true;
      }
    }
  }

  onPointerMove2(event) {
    for (let i = 0; i < this.XRScene.intersects.length; i++) {
      if (this.XRScene.intersects[i].object.uuid == this.figura.object.uuid) {
        let video = this.figura.getVideo();
        if (this.isPlaying == true) {
          video.pause();
          this.isPlaying = false;
        }
      }
    }
  }
}
