import { plano3D } from "./../models/geometry.js";

export class ObjectController {
  constructor(XRScene, object) {
    this.XRScene = XRScene;
    this.object = object;

    window.addEventListener("pointermove", this.onPointerMove.bind(this));
  }
  /* Evalua en cada movimiento del raton (dedo) el contenido de this.XRScene.intersects
   *  this.XRScene.intersects se guardan TODOS los objetos que tocan el dedo del usuario
   */

  // Cuando le das tap en el movil haces esto.
  onPointerMove(event) {
    //https://github.com/mrdoob/three.js/blob/master/manual/examples/webxr-point-to-select.html
    for (let i = 0; i < this.XRScene.intersects.length; i++) {
      if (this.XRScene.intersects[i].object.uuid == this.object.uuid) {
        this.XRScene.intersects[i].object.material.color.set(0xff0000);
      }
    }
  }
}