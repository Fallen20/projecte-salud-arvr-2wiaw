import { plano3D } from './../models/geometryMemory.js';
import { ObjectController_M } from './../controllers/controler.js';//el controlador del onclick para la animacion


export class ObjectController_Memory {
    
    constructor(XRScene, figura,controlador) {
        this.XRScene = XRScene;
        this.figura = figura;
        this.rotate=0;
        this.controlador=controlador;

        window.addEventListener('pointerdown', function (event) {
            this.onClick(event)
        }.bind(this));

    }
    /* Evalua en cada movimiento del raton (dedo) el contenido de this.XRScene.intersects
    *  this.XRScene.intersects se guardan TODOS los objetos que tocan el dedo del usuario
    */

    // Cuando le das tap en el movil haces esto.

    onClick() {

        // console.log("aa");
        //https://github.com/mrdoob/three.js/blob/master/manual/examples/webxr-point-to-select.html
        for (let i = 0; i < this.XRScene.intersects.length; i++) {
            if (this.XRScene.intersects[i].object.uuid == this.figura.object.uuid) {
                // this.casted(this.figura)
                // console.log(this.figura)

                if(this.rotate!=0){
                    this.figura.animateObject(this.controlador);
                }

                // esto es para que el primer click no haga cosas raras porque pilla el click al botón como uno a un objeto y lo gira :/
                // por eso el 9 se gira solo, porque está en esa posición
                this.rotate=1;


                
            }
            
        }
    }

    //Accion a realizar en caso de que coincida. Esta función es la que se debe sobrescribir
    casted(figura) {
        figura.animateObject(this.controlador);
    }

    

}


