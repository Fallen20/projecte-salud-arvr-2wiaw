import * as funcionesContadores from "./../time_functions.js";


export class ControllerMemory{
    arrayGirados=[];
    arrayCompletados=[];
    
    addId(objeto) {
        
        this.arrayGirados.push(objeto);
        
        //has girado dos, miralos
        if(this.arrayGirados.length==2){this.checkId();}
    }
    
    removeId(objeto){
        this.arrayGirados.pop(this.arrayGirados.indexOf(objeto));
    }
    
    checkId() {

        let that=this;

        if(this.arrayGirados[0].id==this.arrayGirados[1].id){
            //hacemos que no pueda girar
            this.arrayGirados[0].canRotate=false;
            this.arrayGirados[1].canRotate=false;

            //los quitamos de lo girado
            this.arrayGirados.pop(this.arrayGirados.indexOf(this.arrayGirados[0]));
            this.arrayGirados.pop(this.arrayGirados.indexOf(this.arrayGirados[1]));
            
            ///los añadimos a los compleetados
            this.arrayCompletados.push(this.arrayGirados[0]);
            this.arrayCompletados.push(this.arrayGirados[1]);
        }
        else{
            //no son el mismo, girarlos pero tras un segundo
            setTimeout(function(){
                // self.arrayGirados[0].object.animateReverseExternal();
                that.arrayGirados[0].animateReverseExternal(that.arrayGirados[0]);
                that.arrayGirados[1].animateReverseExternal(that.arrayGirados[1]);

                //quitarlo de la array
                that.arrayGirados.pop(that.arrayGirados.indexOf(that.arrayGirados[0]));
                that.arrayGirados.pop(that.arrayGirados.indexOf(that.arrayGirados[1]));
            },1000);
        }
        
        //mirar si no están ya todas giradas
        if(this.arrayCompletados.length==12){this.gameWin();}
    }
    

    gameWin(){
        funcionesContadores.saveLocalstorageMemory();

        let frasesWin=[
            'El hospital más antiguo del mundo todavía en funcionamiento se encuentra en Marruecos, y fue fundado en el siglo IX',
            'Las enfermeras utilizan en promedio 600 pares de guantes al mes',
            'El estetoscopio fue inventado en 1816 por un médico francés llamado René Laennec',
            'El corazón humano late alrededor de 100,000 veces al día.',
            'El ADN humano tiene una longitud de aproximadamente 2 metros, pero está comprimido en un núcleo celular que tiene solo unos pocos micrómetros de tamaño.'
            ,'El dolor de cabeza es la razón más común por la cual las personas visitan a un médico.', 'El esqueleto humano está compuesto por alrededor de 206 huesos.',
            'Las personas que tienen un sentido del humor más desarrollado tienen menos probabilidades de enfermarse o tener problemas de salud mental.'
            ,'El cuerpo humano puede sobrevivir varias semanas sin comer, pero solo unos pocos días sin agua.',
            'Robert Liston fue un famoso cirujano del siglo XIX que realizó una operación de amputación en la que murieron el paciente, su ayudante y un espectador debido a las complicaciones de la cirugía. '
        ];

        //random
        document.getElementById("sabiasQueFrase").innerHTML=frasesWin[Math.floor(Math.random() * frasesWin.length)];
        //cuando ganas que salga esto
        document.getElementById("winner").style.opacity=1;
        document.getElementById("winner").style.transition="1s";
        
        document.getElementById("overlay").style.zIndex=100;
        
        document.getElementById("boton-return").style.pointerEvents='all';//el boton se puede pulsar ahora
        
    }
    

    getArrayGiradosLength(){return this.arrayGirados.length;}
    getArrayCompletadosLength(){return this.arrayCompletados.length;}

}