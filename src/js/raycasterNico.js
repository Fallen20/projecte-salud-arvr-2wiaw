import "../css/style.css";
import "../css/buttonStyles.css";

import XRScene from "./views/XRscene";
import { PuzzleController } from "./controllers/PuzzleController";
import { puzzlebox } from "./models/puzzlebox";
import * as funcionesContadores from "./time_functions.js";

const folderName = localStorage.getItem("eleccionPuzle");

var xrScene = new XRScene();
var puzzle = new puzzlebox(xrScene, folderName);
if (!puzzle.isSolvable()) location.reload();

let game = new PuzzleController(xrScene, puzzle);

//SWIPE CONTROL

document.getElementById("ARButton").addEventListener("click", function () {
  document.getElementById("dom-box-left").style.display = "block";
});

let touchstartX = 0;
let touchendX = 0;
let touchstartY = 0;
let touchendY = 0;

document.addEventListener("touchstart", (e) => {
  touchstartX = e.changedTouches[0].screenX;
  touchstartY = e.changedTouches[0].screenY;
});

document.addEventListener("touchend", (e) => {
  touchendX = e.changedTouches[0].screenX;
  touchendY = e.changedTouches[0].screenY;
  game.checkDirection(touchstartX, touchendX, touchstartY, touchendY);
});

const imgPath = require(`./../img/puzzle/${folderName}/full.jpg`);

//FULL IMAGE

const img = new Image();
img.src = imgPath;
img.width = "100";
document.getElementById("dom-box-left").appendChild(img);

//EMPEZAR TIEMPO
funcionesContadores.iniciarIntervalPuzzle();

