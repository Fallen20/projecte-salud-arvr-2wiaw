/*Otras librerias qu epodriamos cargar*/
//import * as ThreeMeshUI from "three-mesh-ui";
//import * as THREEx from '@ar-js-org/ar.js/three.js/build/ar-threex.js';

// import "../css/style.css";
import "../css/styles_Memory.css";
import * as funcionesContadores from "./time_functions.js";
import XRScene from './views/XRscene';//la escena
import { plano3D } from './models/geometryMemory.js';//para hacer el plano
import { ObjectController_Memory } from './controllers/ObjectControllerMemory.js';//el controlador del onclick para la animacion
import { ControllerMemory } from './controllers/ControllerMemory.js';//el controlador del onclick para la animacion


//baraja 1
import b1_1 from "./../img/memory/baraja1_1.png";
import b1_2 from "./../img/memory/baraja1_2.png";
import b1_3 from "./../img/memory/baraja1_3.png";
import b1_4 from "./../img/memory/baraja1_4.png";
import b1_5 from "./../img/memory/baraja1_5.png";
import b1_6 from "./../img/memory/baraja1_6.png";


//baraja 2
import b2_1 from "./../img/memory/baraja2_1.png";
import b2_2 from "./../img/memory/baraja2_2.png";
import b2_3 from "./../img/memory/baraja2_3.png";
import b2_4 from "./../img/memory/baraja2_4.png";
import b2_5 from "./../img/memory/baraja2_5.png";
import b2_6 from "./../img/memory/baraja2_6.png";

//baraja 3
import b3_1 from "./../img/memory/baraja3_1.png";
import b3_2 from "./../img/memory/baraja3_2.png";
import b3_3 from "./../img/memory/baraja3_3.png";
import b3_4 from "./../img/memory/baraja3_4.png";
import b3_5 from "./../img/memory/baraja3_5.png";
import b3_6 from "./../img/memory/baraja3_6.png";


//atras
import atras from "./../img/memory/back.png";


//Cargamos la SCENA
const xrScene = new XRScene();


//---------------------------
    let arrayCompleta=[];

    //crear arrays con cartas
    let baraja1=[b1_1,b1_2,b1_3,b1_4,b1_5,b1_6];
    let baraja2=[b2_1,b2_2,b2_3,b2_4,b2_5,b2_6];
    let baraja3=[b3_1,b3_2,b3_3,b3_4,b3_5,b3_6];

    //las juntas
    let arrayImagenesUsadas=[baraja1,baraja2,baraja3];

    //numero random
    let random=Math.floor(Math.random() * arrayImagenesUsadas.length);

    //sacas una
    let arrayUsada=arrayImagenesUsadas[random];



    //POSICIONES
    let arrayPosiciones=[];

    for (let contador = -5; contador < 6; contador+=5) {
        for (let contador2 = -2.5; contador2 < 6; contador2+=2.5) {
            arrayPosiciones.push([contador,contador2]);
        }
    }
    
    let controlador=new ControllerMemory();
    
    //hacer el shuffle para que cada vez sea random
    arrayPosiciones = arrayPosiciones.sort((a, b) => 0.5 - Math.random());

    //CREAR PLANOS
    //crear los primeros 6 planos
    for (let contador = 0; contador < arrayUsada.length; contador++) {

        // generar los planos. Como no se puede hacer clone o copy se crean 2 que son los pares
        let plano=new plano3D(atras,arrayUsada[contador],xrScene);
        let plano2=new plano3D(atras,arrayUsada[contador],xrScene);
        
        //darle una id que es el contador
        plano.giveId(contador);
        plano2.giveId(contador);

        //lo guardas en la array con todos los planos
        arrayCompleta.push(plano);
        arrayCompleta.push(plano2);
    }


    
    //PONERLOS EN POSICION
    arrayCompleta.forEach((element, contador) => {
        //pillar posiciones
        let x=arrayPosiciones[contador][1];
        let y=arrayPosiciones[contador][0];
        
        //ponerlo en la posicion
        element.object.position.set(x, y, -20)//.applyMatrix4( xrScene.camera.matrixWorld);
        // element.setPosition(x, y, -20, xrScene.controller.matrixWorld);
        

        //darle el controler de movimiento
        new ObjectController_Memory(xrScene, element,controlador);
        
        //meterlo en la escena
        xrScene.scene.add(element.get());

    });
        
     

    //-------------------------------------------------

    //recuperar cosas
    let tituloBaraja=document.getElementById("tituloBaraja");
    let imgBaraja=document.getElementById("barajaImagen");

    //poner baraja
    switch(random){
        case 0:
            tituloBaraja.innerHTML="Instrumentos médicos";
            imgBaraja.src=b1_1;
            break;
        case 1:
            tituloBaraja.innerHTML="Personal médico";
            imgBaraja.src=b2_1;
            break;
        case 2:
            tituloBaraja.innerHTML="Síntomas";
            imgBaraja.src=b3_1;
            break;
        default:
            break;
    }

    //ocultar panel
    document.getElementById("botonClose").addEventListener("click",function () {
        //se cierra
        document.getElementById("infoDiv").hidden=true;

        //inicia el contador cuando cierras
        funcionesContadores.iniciarIntervalMemory();
    });

