/*Otras librerias qu epodriamos cargar*/
//import * as ThreeMeshUI from "three-mesh-ui";
//import * as THREEx from '@ar-js-org/ar.js/three.js/build/ar-threex.js';

import XRScene from './views/XRscene';

import { cono3D } from './models/geometry';
import { ObjectController } from './controllers/ObjectController';

//Cargamos la SCENA
const xrScene = new XRScene();
/*
En el objetto de la scena xrScene tenemos todos los elementos necesarios
xrScene.scene: Para añadir elementos
xrScene.controller: Para manerjar los eventos del usuario
xrScene.camera: Para cambiar parametros de la camara
*/

/*Mostramos la caja de texto para comprobar que podemos superponer elementos DOM*/

/*Mostramos la caja de texto para comprobar que podemos superponer elementos DOM*/
document.getElementById('dom-box').style.display = "block";

//Insertamos un objeto3D en el punto 0,0,0 respecto el usuario cada cez que hace click a un boton
//Como tenemos que leerlo de un fichero, la funcion debe ser asyncrona

let contCones = 0;
document.getElementById('addSilla').addEventListener("click", () => {
	var cono = new cono3D();
	//Solo se interactua con los conos "par". Los impares no cambian de color
	if (contCones % 2 == 0){
		console.log('par')
		new ObjectController(xrScene, cono.object);
	}
	cono.setPosition(0, 0, - 0.3, xrScene.controller.matrixWorld);
	xrScene.scene.add(cono.object);
	contCones++;
})


