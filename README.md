
# Explicación sobre el proyecto a tratar.

El FPlab es un proyecto de innovación impulsado por el Área Metropolitana de Barcelona y la Fundación BCN de Formación Profesional, con el objetivo de poner en valor el talento de los alumnos de formación profesional y acercarlos al mercado laboral, potenciando la adquisición de competencias claves y fomentando su inserción laboral a través de la metodología de innovación abierta en un entorno real.

Nuestro proyecto tiene como objetivo abordar el desafío propuesto por el Hospital del Maresme de Mataró para mejorar la experiencia de los pacientes en las salas de espera de hospitales y consultorios médicos, reduciendo los tiempos de espera y proporcionando un ambiente más favorable y ameno.

# Explicación del proceso de llegar a la conclusión de hacer nuestro proyecto.

Para mejorar el tiempo de espera de los pacientes y darle más valor a éste tiempo decidimos aplicar una solución en la que utilizáramos realidad aumentada. Para ello hicimos una lluvia de ideas y llegamos a la conclusión de que una de las maneras más básicas y atemporales de entretenimiento siempre ha sido el juego. Quisimos aportar algo de valor extra haciendo que nuestros juegos tuvieran algún tipo de relación con la salud y/o el centro, mediante imágenes y textos informativos entre partida y partida.


Para captar el interés de los jugadores más competitivos añadimos un sistema de rankings para comparar el rendimiento en cada juego con las propias partidas anteriores o con los demás jugadores.
Consideramos que una aplicación de juegos de realidad aumentada añade valor también al espacio ya que mientras se juega, a pesar de estar viendo la pantalla, no dejarán de estar viendo la realidad que los rodea, que es la sala del centro de salud en el que están.

Esto puede ser especialmente beneficioso en espacios de espera donde hay paredes de cristal con vistas o no se quiere perder de vista la puerta donde se hacen las visitas o algún otro elemento del hospital.

  
# Tecnología usada

La tecnología usada para este proyecto ha sido la tecnología VR/AR, usando el lenguaje de programación Javascript como base para todo, además de HTML para crear las páginas.

Hemos usado los siguientes programas y frameworks:

- **Aframe**: Usado al principio junto a ThreeJS para poder usar la tecnología VR/AR
- **ThreeJS**: El framework de Javascript final usado para poder usar la tecnología VR/AR
- **WebXR**: Una extensión del navegador que nos permitía visualizar los cambios de realidad virtual que hacíamos
- **NodeJS**: Usada para la estructura de ficheros
- **Visual Studio code**: Editor de código para trabajar
- **Webpack**: Paquete de módulos de Javascript para poder construir archivos
- **Convertidores y editores de video online**: Para poder tener el formato correcto en los videos usados
- **Gitlab**: Para poder almacenar el proyecto
- **Pages**: Integración del git que permite visualizar el proyecto online sin necesidad de plugins
- **Photopea**: Para arreglar imágenes
- **Canva**: Para poder hacer el plafón grande
- **Google drive**: Para compartir y trabajar los proyectos de FPlab

# Explicacion de los elementos
## Puzzle

Uno de los juegos que pensamos era un puzzle. Al ser movimiento y posicionamiento de elementos en el espacio era una muy buena idea para un proyecto de juego en realidad aumentada. Sin embargo el movimiento de los objetos en ThreeJS con transiciones agradables era un tema complicado cuando empezamos el proyecto, así que llegamos a la conclusión de que hacer un “Slider Puzzle” era una mejor idea. 


En un slider puzzle tienes piezas desordenadas de N\*M con una pieza faltante para dar espacio de movimiento a cada pieza, donde solo se pueden mover por la matriz N\*M en la que han sido posicionadas.

El jugador debe usar su ingenio para mover las piezas, aprovechando el espacio libre, con el fin de resolver el acertijo posicionando cada pieza en el sitio que toca. 

Hay veces que las piezas pueden ponerse en un orden que hace el puzle irresoluble. Ésto ocurre cuando hay un puzle N\*M donde N y M son el mismo número impar y hay un número de transiciones impar, impidiendo que pueda ser resuelto. Así que para asegurarnos que el puzzle es resoluble hay que comprobar que tenga un número par de inversiones, si no, volver a cargar el puzle.

El número de **inversiones de una matriz** se refiere al número de parejas de elementos de la matriz que están en orden inverso en relación a su posición en una matriz ordenada de manera creciente.

Por ejemplo en una matriz cuadrada 2\*2 su orden original es :  
1,2  
3,4

Si por ejemplo se presenta de esta manera:   
1,4  
3,2

Hay 3 inversiones en total: el (4, 3), (4, 2) y (3, 2) en posiciones invertidas

Con la función isSolvable dentro de la clase puzzleBox se comprueba si el puzle se puede resolver. Si no es posible se vuelve a cargar el puzle, repitiendo el proceso hasta que el resultado es positivo.

```js

isSolvable() {

       let count = 0;

       for (let i = 0; i < this.puzzle.length; i++) {

         for (let j = i + 1; j < this.puzzle.length; j++) {

           if (this.puzzle[i].num > this.puzzle[j].num) {

             count += 1;

           }

         }

       }

       return count % 2 == 0;

     }
```   


Para realizar el movimiento con una animación de traslación y que no saltara de un sitio a otro, se ha utilizado Tween.js, una librería de Javascript que funciona muy bien con Three.js y permite realizar movimientos con animación de una manera relativamente sencilla.

```js
     move(x, y) {

       let puzzle = this.puzzle;
       
       this.xrScene.selected.tween = 
            new TWEEN.Tween(this.xrScene.selected.position);

       this.xrScene.selected.tween.to(

         {

           x: Math.round((this.xrScene.selected.position.x + x) \* 10) / 10,

           y: Math.round((this.xrScene.selected.position.y + y) \* 10) / 10,

         },

         100

       );

       this.xrScene.selected.tween.start();

       setTimeout(function () {

         puzzle.isSolved();

       }, 500);

     }
```

Para que el movimiento **Tween** funcione debe escribirse “TWEEN.update();” esto dentro de la función animate de la escena.

```js
     requestAnimationFrame(function animate(nowMsec) {

         // keep looping

         requestAnimationFrame(animate);

         TWEEN.update();

         // measure time

         lastTimeMsec = lastTimeMsec || nowMsec - 1000 / 60;

         var deltaMsec = Math.min(200, nowMsec - lastTimeMsec);

         onRenderFcts.forEach(function (onRenderFct) {

           onRenderFct(deltaMsec / 1000, nowMsec / 1000);

         });

       });
```
         

El planteamiento para el sistema de movimiento y bloqueo de los bloques del puzzle está realizado de manera que cada puzzle ocupa una posición en el espacio, y antes de realizar los movimientos se realizan dos comprobaciones:
\- Si la posición X o Y del sitio al que se va a mover esta fuera de los “boundaries” marcados
\- Si en la posición X e Y del sitio al que se va a mover ya hay un bloque.

Las siguientes funciones devuelven un objeto y dependiendo de hacia dónde se quiera mover, por ejemplo para mover hacia la izquierda, se debe acceder al atributo **left** del objeto que devuelve la función, llamándolo de esta manera:

if (this.canMove(this.xrScene).left && this.canMoveOut(this.xrScene).left)

Las funciones que comprueban estos casos son:

```js

canMoveOut(xrScene) {

       let canMove1 = {};

       canMove1.left = xrScene.selected.position.x - 0.1 > -0.2;

       canMove1.right = xrScene.selected.position.x + 0.1 < 0.2;

       canMove1.down = xrScene.selected.position.y - 0.1 > -0.2;

       canMove1.up = xrScene.selected.position.y + 0.1 < 0.2;

       return canMove1;

     }

     canMove(xrScene) {

       let canMove = {};

       canMove.left = !this.puzzle.puzzle.some(

         (obj) => obj.position.x == xrScene.selected.position.x - 0.1 && 
            obj.position.y == xrScene.selected.position.y

       );

       canMove.right = !this.puzzle.puzzle.some(

         (obj) => obj.position.x == xrScene.selected.position.x + 0.1 && 
            obj.position.y == xrScene.selected.position.y

       );

       canMove.down = !this.puzzle.puzzle.some(

         (obj) => obj.position.x == xrScene.selected.position.x && 
            obj.position.y == xrScene.selected.position.y - 0.1

       );

       canMove.up = !this.puzzle.puzzle.some(

         (obj) => obj.position.x == xrScene.selected.position.x && 
            obj.position.y == xrScene.selected.position.y + 0.1

       );

       canMove.block = this.puzzle.puzzle.some(function (obj) {

         return obj.position != xrScene.selected.position;

       });

       return canMove;

     }
```   

Una vez hechas estas comprobaciones  si la comprobación da false no se permite mover el bloque. Al contrario, si el resultado es true, se puede mover a esa posición.

Para comprobar que cada bloque está en el sitio que les corresponde cada objeto pieza que se ha creado tienen los atributos añadidos **basex** y **basey** que indican la posición final que debe tener ese objeto. 

```js
for (let i = 1; i >= -1; i--) {

         for (let j = -1; j <= 1; j++) {

           images.push({ img: imgs[cont], basex: j * proporcion, basey: i *
             proporcion, num: cont + 1 });

           cont++;

         }

       }
```       

En cada movimiento se lanza la comprobación de si los elementos están en el sitio final, recorriendo los objetos del puzzle y comparando su posición real con la posición base que tienen guardada como atributo.

```js
     isSolved() {

       let solved = this.puzzle.every((a) => {

         return a.position.x == a.homex && a.position.y == a.homey;

       });

       if (solved) {

         this.loadWinText();

       }

     }
```        

Si se resuelve, se muestra un div html con un consejo aleatorio de una array sobre la salud, y un botón para volver al menú.

Respecto a las imágenes, cada imagen de puzzle está guardada en una subcarpeta con un nombre identificatorio. Dentro de éstas carpetas hay los trozos de la imagen dividida en 9, y una imagen del puzzle entero para usar como guía.

A la hora de entrar al puzzle antes se debe seleccionar en la interfaz un tipo de puzzle en un select que contiene los nombres de las carpetas existentes que contienen las imágenes de cada puzzle. Una vez se hace click al botón de jugar, se guarda en una variable el valor seleccionado y ésa variable es utilizada dentro del código para acceder a la carpeta con ese nombre. Esto se consigue añadiendo la variable al string de ruta a la hora de cargar las imágenes donde toca. 

En el html se carga la imágen entera y su div se vuelve visible para que sirva de guía al usuario y así sepa cómo debe quedar el puzzle una vez resuelto.

La función para cargar las imágenes en las piezas es la siguiente:

```js
     loadImgs(folderName) {

       var imgs = [];

       for (let i = 1; i <= 8; i++) {

         try {

           imgs.push(require(   ../../img/puzzle/${folderName}/${i}.jpg   ));

         } catch (err) {

           imgs.push(require(   ../../img/puzzle/${folderName}/${i}.png   ));

         }

       }

       return imgs;

     }
```         

Código para mostrar la imagen del puzzle entero en el html.

``` javascript

const imgPath = require(   ./../img/puzzle/${folderName}/full.jpg   );

//FULL IMAGE

const img = new Image();

img.src = imgPath;

img.width = "100";

document.getElementById("dom-box-left").appendChild(img);
```
         


Para detectar qué bloque está seleccionado se utiliza el raycaster. Cada pieza de puzzle tiene un object controller en el cual cuando mueves el puntero se lanza un raycaster. Ese raycaster recoge el número de objetos con los que hace intersección.

Se compara si de los objetos con los que se ha hecho intersección hay uno que coincide con el propietario del controller, y si es así el atributo **emissive** del material del objeto dueño del controller se vuelve verde. Antes de hacer esto, recorre todos los objetos del puzle y les pone el atributo **emissive** a 0 para que los objetos anteriormente seleccionados pierdan ese color.

Así el objeto seleccionado con el puntero y el raycaster es el que emite un color verde, y el resto están claramente sin seleccionar. 
El objeto seleccionado se guarda como atributo de la escena para poder ser manipulado más fácilmente desde otras partes del código.

La función del Object controller de cada pieza:

``` javascript

     onPointerMove(event) {

       for (let i = 0; i < this.XRScene.intersects.length; i++) {

         if (this.XRScene.intersects[i].object.uuid == this.object.uuid) {

           this.puzzle.forEach((object, i, array) => {

             array[i].material.emissive.set(0x000000);

           });

           this.XRScene.intersects[i].object.material.emissive.set(0x00ff00);

           this.XRScene.selected = this.XRScene.intersects[i].object;

         }

       }

     }
```        

Para mover el bloque seleccionado al principio se usaba una cruceta en html con los botones de arriba, abajo, izquierda, y derecha, y se movía el objeto en la dirección seleccionada.
Sin embargo la intención fue siempre la de hacer que fuese un movimiento “on touch”, pero por el desconocimiento de Three.js y su complejidad se tuvo que dejar para el final. 

Finalmente se consiguió lograr el funcionamiento de movimiento de piezas al tocarla y mover el dedo mediante un detector de “swipes”

Para detectar los swipes:

``` javascript

let touchstartX = 0;

let touchendX = 0;

let touchstartY = 0;

let touchendY = 0;

document.addEventListener("touchstart", (e) => {

     touchstartX = e.changedTouches[0].screenX;

     touchstartY = e.changedTouches[0].screenY;

});

document.addEventListener("touchend", (e) => {

     touchendX = e.changedTouches[0].screenX;

     touchendY = e.changedTouches[0].screenY;

     game.checkDirection(touchstartX, touchendX, touchstartY, touchendY);

});
```
         


Ahora el funcionamiento era detectando el movimiento que desea el usuario detectando esos “swipes” mediante una serie de funciones se detecta si el “swipe” que se realiza en la pantalla va en un determinado sentido y dirección y “triggerea” en el objeto **seleccionado** un movimiento u otro, dependiendo del swipe.

Para hacer una cosa u otra dependiendo del swipe: 

``` javascript

     checkDirection(touchstartX, touchendX, touchstartY, touchendY) {

       if (touchendX < touchstartX) {

         if (this.canMove(this.xrScene).left && 
            this.canMoveOut(this.xrScene).left) {

           this.move(-0.1, 0);

         }

       }

       if (touchendX > touchstartX) {

         if (this.canMove(this.xrScene).right && 
            this.canMoveOut(this.xrScene).right) {

           this.move(0.1, 0);

         }

       }

       if (touchendY < touchstartY) {

         if (this.canMove(this.xrScene).up && this.canMoveOut(this.xrScene).up) {

           this.move(0, 0.1);

         }

       }

       if (touchendY > touchstartY) {

         if (this.canMove(this.xrScene).down && 
            this.canMoveOut(this.xrScene).down) {

           this.move(0, -0.1);

         }

       }

     }
```
    
## Memory

Memory es un juego que plantea el poder ejercitar la memoria del usuario con tres tipos de barajas, dando un toque educativo con las imágenes fácilmente reconocibles e identificables y con un detalle curioso del sector de la medicina una vez completado.

La idea principal era el poder distraer a los pacientes con un juego clásico, fácil de jugar y reconocer y, a la vez, poder proporcionar ese toque educativo extra que tanto queríamos.


Se han usado varios algoritmos, como el de la creación de elementos y hacer una acción u otra tras la comprobación de la cartas giradas.

Uno ejemplo de uno de ellos es el de la generación aleatoria de posiciones:

1. Sacar un número aleatorio que servirá para elegir la baraja

2. Crear y barajar las posiciones en donde estarán los cuadrados

3. Crear los cuadrados necesarios recorriendo la array de imágenes

4. Asignar las posiciones a los cuadraods mientras se asignan los controladores y se agregan a la escena


```javascript
//numero random

      let random=Math.floor(Math.random() \* arrayImagenesUsadas.length);

        //sacas una

        let arrayUsada=arrayImagenesUsadas[random];



        //POSICIONES

        let arrayPosiciones=[];

        for (let contador = -5; contador < 6; contador+=5) {

            for (let contador2 = -2.5; contador2 < 6; contador2+=2.5) {

                arrayPosiciones.push([contador,contador2]);

            }

        }



        let controlador=new ControllerMemory();



        //hacer el shuffle para que cada vez sea random

        arrayPosiciones = arrayPosiciones.sort((a, b) => 0.5 - Math.random());

        //CREAR PLANOS

        //crear los primeros 6 planos

        for (let contador = 0; contador < arrayUsada.length; contador++) {

            // generar los planos. Como no se puede hacer clone o 
            // copy se crean 2 que son los pares

            let plano=new plano3D(atras,arrayUsada[contador],xrScene);

            let plano2=new plano3D(atras,arrayUsada[contador],xrScene);



            //darle una id que es el contador

            plano.giveId(contador);

            plano2.giveId(contador);

            //lo guardas en la array con todos los planos

            arrayCompleta.push(plano);

            arrayCompleta.push(plano2);

        }




        //PONERLOS EN POSICION

        arrayCompleta.forEach((element, contador) => {

            //pillar posiciones

            let x=arrayPosiciones[contador][1];

            let y=arrayPosiciones[contador][0];



            //ponerlo en la posicion

            element.object.position.set(x, y, -20)
            //.applyMatrix4( xrScene.camera.matrixWorld);

            // element.setPosition(x, y, -20, xrScene.controller.matrixWorld);



            //darle el controler de movimiento

            new ObjectController\_Memory(xrScene, element,controlador);



            //meterlo en la escena

            xrScene.scene.add(element.get());

        });
```
       

El flujo de trabajo que se ha seguido ha sido:

1. Creación de los archivos JS y HTML necesarios

2. Modificación del webpack

3. Creación de elementos 3D the ThreeJS

4. Creación de la básica página HTML 

5. Creación de elementos en archivo JS

6. Controladores para detectar el toque a ese elemento

7. Animaciones individuales

8. Creación de comprobaciones

9. Algoritmo para la limpieza y eficiencia del código general

10. Separación de métodos en diferentes controladores

11. Substitución de imágenes de prueba

12. Agregación de elementos finales en el HTML con sus métodos

13. Agregación de los métodos HTML en los controles correspondientes

14. Fusión con la rama principal

Para la crear el objeto se tenían que crear unas texturas para cada lado. Para ello, en el JS donde creábamos los cubos importamos las imágenes necesarias

``` js
import b1_1 from "./../img/memory/baraja1_1.png";
```

Y a la hora de crear el propio objeto 3D se mandan como parte del constructor

``` js
let plano=new plano3D(atras,arrayUsada[contador],xrScene);
```

En el propio objeto se usaron dos funciones, una que cargaba la textura y otro que cargaba un color sólido, en este caso negro

``` js
 //creamos texturas
      let back = this.setTexture(atras);
      let front = this.setTexture(frente);
      let sides = this.setSideTexture();
  
      var cubeMaterialArray = [sides, sides, sides, sides, front, back];
      // var cubeMaterialArray = [sides,sides,sides,sides,sides,sides];
  
      this.object = new THREE.Mesh(geometry, cubeMaterialArray);
      // this.object = new THREE.Mesh(geometry, back);
  
    setTexture(imagen) {
      const textura = new THREE.TextureLoader().load(imagen);
      const material = new THREE.MeshBasicMaterial({ map: textura });
  
      return material;
      // return new THREE.MeshBasicMaterial( {color: 0x000000} );//negro
    }
  
    setSideTexture() {
      return new THREE.MeshBasicMaterial({ color: 0x00000 }); //magenta
    }

```

El modelo del objeto también era el encargado de las animaciones del mismo.

Con 'animateObject' se controlaba cual de los dos animaciones era la que se debía hacer (frente a espaldas o a la inversa)
``` js
animateObject(controler){
      
      if(!this.isRotated && this.canRotate){this.animate(controler);}
      else if(this.isRotated && this.canRotate){this.animateReverse(controler);}
    }

```

Y ambos métodos usaban el método de la escena `onRenderFcts`, que permitía hacer la animación mucho más suave que cualquier método con bucles. 
Según si era inverso o frente, se quitaba o se añadía a la array del controlador correspondiente.

```js
animate(controler) {
      
      this.xrScene.onRenderFcts[this.xrScene.onRenderFcts.length] = 
        function (delta, now) {
        
        if (this.object.rotation.y < 3.2){
          this.object.rotation.y += 1 * delta / 2;
        }
        else {
          this.xrScene.onRenderFcts.pop();
        }
      }.bind(this);
      
      this.isRotated=true;

      
      controler.addId(this);
    }
    
    animateReverse(controler) {
    
      this.xrScene.onRenderFcts[this.xrScene.onRenderFcts.length] = 
        function (delta, now) {
            if (this.object.rotation.y > 0) {
            this.object.rotation.y -= 1 * delta/2;
            }
            else {
            this.xrScene.onRenderFcts.pop();
            }
        }.bind(this);
        this.isRotated = false;
        
        controler.removeId(this);
    }

```

Los controladores se usaron para poder controlar el objeto que el rayo que el raycaster creaba. El evento usado para lanzar la función fue 'pointerdown' en relación a hacer un toque. Había otros eventos que podrían usarse (click, pointerup, mouseover), pero solo uno de ellos funciona de la manera requerida.
``` js
export class ObjectController_Memory {
    
    constructor(XRScene, figura,controlador) {
        this.XRScene = XRScene;
        this.figura = figura;
        this.rotate=0;
        this.controlador=controlador;

        window.addEventListener('pointerdown', function (event) {
            this.onClick(event)
        }.bind(this));

    }

    onClick() {

        for (let i = 0; i < this.XRScene.intersects.length; i++) {
            if (this.XRScene.intersects[i].object.uuid == 
                this.figura.object.uuid) {

                if(this.rotate!=0){
                    this.figura.animateObject(this.controlador);
                }

                this.rotate=1;


                
            }
            
        }
    }
}

```

El otro controlador usado fue para la comprobación de los elementos una vez girados.
Se crearon métodos simples para añadir y borrar de la array una vez se giraban
``` js
addId(objeto) {
        
        this.arrayGirados.push(objeto);
        
        //has girado dos, miralos
        if(this.arrayGirados.length==2){this.checkId();}
    }
    
    removeId(objeto){
        this.arrayGirados.pop(this.arrayGirados.indexOf(objeto));
    }

```

Y luego el comprobante una vez se giraban dos:
Al principio detecta si son el mismo. Si es el caso, se cambia la propiedad 'canRotate' para evitar que se pueda rotar de nuevo. Tras eso lo quitamos de la array de comprobación y los agregamos a la de completados, que es la que se comprobará para sacar el método 'gameWin'.
Si es el caso de no ser el mismo, se ejecuta un timeout que girará las cartas y las quitará de la array. El timeout se hizo para que el jugador tenga tiempo de ver las dos cargas y sea consciente de la posición aunque la pareja sea errónea.

``` js
checkId() {

        let that=this;

        if(this.arrayGirados[0].id==this.arrayGirados[1].id){
            //hacemos que no pueda girar
            this.arrayGirados[0].canRotate=false;
            this.arrayGirados[1].canRotate=false;

            //los quitamos de lo girado
            this.arrayGirados.pop(this.arrayGirados.indexOf(
                this.arrayGirados[0]));
            this.arrayGirados.pop(this.arrayGirados.indexOf(
                this.arrayGirados[1]));
            
            ///los añadimos a los compleetados
            this.arrayCompletados.push(this.arrayGirados[0]);
            this.arrayCompletados.push(this.arrayGirados[1]);
        }
        else{
            //no son el mismo, girarlos pero tras un segundo
            setTimeout(function(){
                // self.arrayGirados[0].object.animateReverseExternal();
                that.arrayGirados[0].animateReverseExternal(
                    that.arrayGirados[0]);
                that.arrayGirados[1].animateReverseExternal(
                    that.arrayGirados[1]);

                //quitarlo de la array
                that.arrayGirados.pop(that.arrayGirados.indexOf(
                    that.arrayGirados[0]));
                that.arrayGirados.pop(that.arrayGirados.indexOf(
                    that.arrayGirados[1]));
            },1000);
        }
        
        //mirar si no están ya todas giradas
        if(this.arrayCompletados.length==12){this.gameWin();}
    }

```

El último método era el que se ejecutaba una vez todas las parejas habían sido giradas. Sacando una frase aleatoria de la salud, hacia visible el div con el botón de volver al inicio, además de parar el contador.
``` js
gameWin(){

        funcionesContadores.saveLocalstorageMemory();

        let frasesWin=["..."];

        //random
        document.getElementById("sabiasQueFrase").innerHTML = 
            frasesWin[Math.floor(Math.random() * frasesWin.length)];
        //cuando ganas que salga esto
        document.getElementById("winner").style.opacity=1;
        document.getElementById("winner").style.transition="1s";
        
        document.getElementById("overlay").style.zIndex=100;
        
        document.getElementById("boton-return").style.pointerEvents='all';
        //el boton se puede pulsar ahora
        
}

```

## Music

Music plantea un juego DJ  sin puntuaciones ni competición. El juego consiste en la mezcla de diferentes sonidos para intentar generar una canción (en este caso electrónica). Usando la tecnología de realidad aumentada con ThreeJS, creamos unos cubos posicionados frente al usuario donde se cargan los videos con los sonidos. El jugador podrá poner play y pausa el cubo a su gusto en cualquier momento, para así mezclar los sonidos e intentar generar una canción en base a sus preferencias.


Primero de todo nosotros generamos un objeto cubo, donde le pasaremos por parámetro al constructor una textura de video, que será un video con el sonido que queramos que tenga y le daremos un tamaño en concreto.

```js

export class cubo3D {
  //esto es una clase
  //y dentro tiene metodos
  constructor(video) {
    this.video = video;
    this.isPlaying = false;


    var geometry = new THREE.BoxGeometry(10, 10, 10); //creas la forma

    //creamos textura
    let texture = new THREE.VideoTexture(video);
    

    //creamos el material
    let materialMovie = new THREE.MeshBasicMaterial({
      map: texture, //darle la textura con el movil
      side: THREE.DoubleSide, //se ve en ambos lados
      toneMapped: false,
    });
    materialMovie.needsUpdate = true;

    this.object = new THREE.Mesh(geometry, materialMovie); //le aplicas esto
  }

  //metodo para ponerlo en una posicion especifica
  setPosition(x, y, z, matrix = null) {
    if (matrix != null) {
      this.object.position.set(x, y, z).applyMatrix4(matrix);
    } else this.object.position.set(x, y, z);

    this.object.visible = true;
  }



```

Luego en el controlador controlará poner el video en reproducción o pausa según las acciones del usuario. Para lograr esto tendremos que comprobar el estado del video y mediante 2 eventos distintos podremos gestionar el cambio de estado. 

```js

export class ObjectControllerMusic {
  constructor(XRScene, figura) {
    this.XRScene = XRScene;
    this.figura = figura;
    this.isPlaying = false;

    window.addEventListener('click', this.onPointerMove.bind(this));
    window.addEventListener("pointermove", this.onPointerMove2.bind(this));
  }

  // Cuando le das tap en el movil haces esto.
  onPointerMove(event) {
    for (let i = 0; i < this.XRScene.intersects.length; i++) {
      if (this.XRScene.intersects[i].object.uuid == this.figura.object.uuid) {
        let video = this.figura.getVideo();
        video.play();
        this.isPlaying = true;
      }
    }
  }

  onPointerMove2(event) {
    for (let i = 0; i < this.XRScene.intersects.length; i++) {
      if (this.XRScene.intersects[i].object.uuid == this.figura.object.uuid) {
        let video = this.figura.getVideo();
        if (this.isPlaying == true) {
          video.pause();
          this.isPlaying = false;
        }


```


Ahora importamos los vídeos que queremos poner como textura a los cubos y se los añadimos para luego guardaros en un array para poder acceder a los videos y ponerlos como VideoTexture a los objetos mediante una serie de bucles.

Estos bucles acceden al id del video y se pone como VideoTexture al cubo, se da una posición estática respecto al jugador y en nuestro caso, el algoritmo los posiciona en 2 columnas. Cargamos los videos con la funciona load(), y por último los controladores que hemos realizado antes.

Y por último,se añaden los videos a nuestra escena que es quien carga todo el contenido de realidad aumentada.

```js
import XRScene from "./views/XRscene";
import { cubo3D } from "./models/geometryMusic";
import videoSintel2 from "../videos/sintel.mp4";
import videoPrueba from "../videos/gandalf.mp4";
import mario from "../videos/mario.mp4";
import drum from "../videos/drum.mp4";
import errorSound from "../videos/error.mp4";
import base from "../videos/base.mp4";

import "../css/buttonStyles.css";
import "../css/styleMusic.css";

import { ObjectControllerMusic } from "./controllers/ObjectControllerMusic.js";

let arrayVideos = [videoPrueba, videoSintel2, mario, drum, errorSound, base];

//Cargamos la SCENA
const xrScene = new XRScene();
// Inicializamos tanto los videos como los cubos en arrays.
const videoEl = [];
const cubo = [];
/*
En el objetto de la scena xrScene tenemos todos los elementos necesarios
xrScene.scene: Para añadir elementos
xrScene.controller: Para manerjar los eventos del usuario
xrScene.camera: Para cambiar parametros de la camara
*/

let cantidad=5;
for (let i = 0; i < arrayVideos.length; i++) {
  videoEl[i] = document.getElementById("video" + i);
}

for (let i = 0; i < arrayVideos.length; i++) {
  videoEl[i].children[0].src = arrayVideos[i];
}

for (let i = 0; i <= cantidad; i++) {
  cubo[i] = new cubo3D(videoEl[i]);
}

// Le ponemos una posicion (x, y, z), si Z es negativo, aparece delante nuestro, si es positivo, aparece detras
let z = -40;
let x = 10;
let y = -20;
let a = 0;
for (let i = 0; i <= cantidad; i++) {
  if (i % 2 == 0) x = 10;
  else x = -10;
  cubo[i].setPosition(x, y, z, xrScene.controller.matrixWorld);
  a++;
  if (a % 2 == 0) y += 20;
}

// Añadimos los cubos a la escena.
for (let i = 0; i <= cantidad; i++) {
  xrScene.scene.add(cubo[i].get());
}

// Cargamos los videos.
for (let i = 0; i <= cantidad; i++) {
  videoEl[i].load();
}

for (let i = 0; i <= cantidad; i++) {
  new ObjectControllerMusic(xrScene, cubo[i]);
}

```

# Interfaz del proyecto.

En nuestro caso hemos optado por una interfaz sencilla e intuitiva, con colores suaves para intentar generar un efecto relajante.

La primera página que se ve es la de login, donde damos la bienvenida al centro hospitalario con su logo en el centro. Justo debajo, un formulario donde lo único que se debe poner es un nombre de usuario para poder guardar las puntuaciones de los juegos. De fondo, una imagen con un “gradient” de tonalidad lila para seguir con al estética.

Una vez hecho el login, la página de home es la que carga. Hay un header y un footer de colores gris oscuro azulado.

En el footer solo está el copyright de la escuela y en el header el pequeño logo de quién propuso el reto junto a un menú hamburguesa que contiene el nombre del usuario y diferentes redirecciones a los rankings de los juegos como así también un botón de home y un logout.

La idea original era poner un cuarto botón con un ranking global, pero al final las puntuaciones pasaron de ser basadas en puntos a ser del tiempo que tardas en resolverlos y no era factible, ya que no podían juntarse.

Cuando accedemos a los rankings, carga una nueva vista donde tenemos el título y una tabla con las puntuaciones de diferentes jugadores. Entre la tabla y el título tendremos la puntuación propia del jugador.

Al no tener al final base de datos, se usó un texto hardcodeado en el apartado script, que creaba el cuadrado con las puntuaciones ordenadas con métodos.

El primer método, ranking, se encargaba de crear la array, generar las puntuaciones aleatorias, no más de 5 minutos en milisegundos y juntar los elementos en una array de dos posiciones, además de comprobar si el jugador tenía tiempo o no, para ponerlo en una posición u otra.

```js      

function ranking() {

    //generar nombres

    let nombres = [...];

    //generar puntuaciones en milisegundos

    let puntuaciones = Array.from({ length: 22 }, () => 
        Math.floor(Math.random() \* 900000));

    //ahora le hacemos un shuffle para que no siempre salga lo mismo

    nombres = nombres.sort((a, b) => 0.5 - Math.random());

    puntuaciones = puntuaciones.sort((a, b) => 0.5 - Math.random());

    //los metemos en un div

    let divRanking = document.getElementById("ranking");

    let arrayFinale = [];

    //array de dos posiciones con nombre y puntuacion, le metes el 
    //nombre propio del jugador

    for (let contador = 0; contador < nombres.length; contador++) {

        const nombre = nombres[contador];

        const puntuacion = puntuaciones[contador];

        arrayFinale.push([nombre, puntuacion]);

    }

```   

El método para crear cada elemento en el div se llama **crearElementoRanking** y haciendo un bucle con la array de puntuaciones creadas pasábamos el nombre, la puntuación y la posición en la array, creando los elementos interiores y dándole el estilo necesario con js.

```js      

function crearElementoRanking(nombre, puntuacion, contador) {

    let divRanking = document.getElementById("ranking");

    const div = document.createElement('DIV');

    const nombreElemento = document.createElement('SPAN');

    const puntuacionElemento = document.createElement('SPAN');

    //le damos el texto

    if (typeof (contador) == "number") {

        nombreElemento.textContent = (contador + 1) + ". " + nombre;

    }

    else {

        nombreElemento.textContent = "—" + ". " + nombre;

    }

    let finale = "-";

    if (puntuacion != null) {

        //pasarlo a minutos y eso

        finale = calculoTiempo(parseInt(puntuacion));

    }

    puntuacionElemento.textContent = finale;

    //damos estilo al div para que estén al lado del otro

    div.style.display = "flex";

    div.style.justifyContent = "space-between";

    //estilo al span

    nombreElemento.style.fontSize = "1.2rem";

    puntuacionElemento.style.fontSize = "1.2rem";

    puntuacionElemento.style.color = "black";

    nombreElemento.style.color = "black";

    //si eres tu, que se vuelva negrita

    if (nombre == localStorage.getItem('nombreUser')) {

        nombreElemento.style.fontWeight = 'bolder';

        nombreElemento.style.color = "red";

    }


    //le damos al div los elementos

    div.appendChild(nombreElemento);

    div.appendChild(puntuacionElemento);

    //se lo damos al div grande

    divRanking.appendChild(div);

}
```        

En el home, el header tiene las puntuaciones que tiene el usuario en los 2 juegos y debajo damos la bienvenida con el nombre de la aplicación y un carrusel con los 3 juegos, que van rotando en un periodo de 5 segundos o a placer del jugador arrastrando el dedo.

El carrusel está compuesto de cards y dentro de cada una está el título del juego con una foto orientativa, una breve explicación de qué va y un botón para acceder.

Cuando le damos a jugar la aplicación redirige a la página del juego correspondiente, donde hay un fondo blanco con una cuadrado con la información sobre cómo se juegao como funciona. En la parte superior hay 2 botones, uno de redirección al home y en el caso de Music mostrar o ocultar la información, y en la parte inferior de la página, un botón que abriría la camara del móvil y el usuario podría ver el juego en realidad aumentada.


# Tiempo

Una vez se unificó todo el proyecto se añadió el elemento competitivo: puntuaciones. Ya que no se podían conseguir puntos de por sí, se decantó por poner marcas de tiempo que empezarían en cuanto cargase la página y se pararían cuando saliese el cuadrado de 'Has ganado'

Para hacer esto se usó un archivo JS 'time\_functions' que lo llamarían las dos páginas principales y cualquier controlador que lo necesitase. Los métodos creados fueron:

```js

export function iniciarIntervalPuzzle() {

        intervalPuzzle = setInterval(() => {

            contadorPuzzle++;

        }, 1000);

}

```

Este método se llamaba cuando se cargaba la página y sumaba 1 a un contador creado antes cada segundo. Un método algo rudimentario para crear un contador.

```js
export function saveLocalstoragePuzzle() {

        clearInterval(intervalPuzzle);

        localStorage.setItem('contadorPuzzle', contadorPuzzle);//guarda segundos

}
```

Este método se llamaba una vez el elemento ganador salía por pantalla, tras ganar el juego. Hacía las funciones de parar el intervalo del juego correspondiente y guardar en localstorage el tiempo en segundos de lo que había tardado el jugador.

En este caso, como no teníamos una base de datos propia, se guardaba en sesiones para mostrar ese solo usuario.

Para mostrar este tiempo en su correspondiente marca (Minutos:Segundos) se usaba este método, llamado en las páginas de ránking y enla de inicio, que también mostraba el tiempo en el navegador superior:

Primero de todo se comprovaba si había un valor previo.

```js
//mirar si no existe

       if (contadorMemory == null || contadorMemory=="0") {

        spanMemory.innerHTML = "-";

      }

      else {

        spanMemory.innerHTML = calculoTiempo(contadorMemory);

      }

```

Si no había, se ponía una raya para indicar que no había puntuación.

Si había, se llamaba al método para calcular tiempo.

```js
function calculoTiempo(milisegundos){

        let finale;



        if(milisegundos<60000){//es menor a 1 minuto

            if(milisegundos<10000){//menor a 10seg

                finale =  "0:0" + Math.floor(milisegundos / 1000);

            }

            else{//no llega al minuto pero se necesita redondear el tiempo y tal

                finale =  "0:" +  Math.floor(milisegundos / 1000);



            }

        }

        else{//mayor a un minuto

            const minutos = parseInt(milisegundos / 1000 / 60);

            milisegundos -= minutos \* 60 \* 1000;

            let segundos = Math.floor(milisegundos / 1000);



            //si los segundos restantes son menos de 10

            if (segundos < 10) { segundos = "0" + segundos; }

            finale = minutos + ":" + segundos;

        }



            return finale;

}
```

Este método cambiaba ligeramente según si se ejecutaba en ranking o home, ya que la creación del ranking ya teníamos los elementos en milisegundos y en el caso del jugador lo teníamos en segundos; la diferencia era el tener que multiplicar por 1000 si era el jugador.

Pero como en el ranking se tenía que hacer la comprobación de saber si había puntuación para poder añadirlo a la array, se podía multiplicar antes de añadirlo. 

En el caso de no haber puntuación, se añadía al final de la array y al no pasarlo a tipo número pasaba por la comprobación de typeof y ponía un guión en la posición y puntuación.


# Problemas encontrados.

- La **precisión** de los decimales de javascript dio algo de problemas con el puzzle. Como el movimiento y la comprobación de posición se hacia con posiciones con decimales, a veces al hacer calculos JavaScript hacía eso de en lugar de dar 0.1, daba 0.10000000006. La solución que encontre a esta falla de javascript fue hacer  





```js
Math.round((NumeroConDecimales) \* 10) / 10
```

Aún haciendo esto hay veces que calcula mal, pero se reduce mucho las posibilidades.

- El **raycaster** nos dio problemas por que estaba definido de manera que utilizaba el documento entero en lugar de la pantalla para crear el rayo. Por ello apuntaba fuera y no era coherente con la posición del toque en la pantalla.
